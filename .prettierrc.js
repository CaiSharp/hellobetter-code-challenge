module.exports = {
	bracketSpacing: false,
	singleQuote: true,
	trailingComma: 'all',
	useTabs: true,
	singleQuote: true,
	endOfLine: 'lf',
};
