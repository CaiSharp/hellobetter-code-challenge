## How to run the app

1. Install all packages with `yarn setup`
2. Run the app with either `yarn ios` or `yarn android`
   - do note that the camera module is only supported on real devices

## How to use the app

1. Select any of the 2 courses from home
2. Watch any of the 2 videos, you can navigate by swiping
3. When the video finishes a few feedback options will be shown, pressing on any option will open a modal
4. All data is being saved automatically when closing the modals or leaving the course

## Tested on

1. Simulators: iPhone 12 , iPhone SE, Google Pixel 3a
2. Hardware: iPhone 11Pro, Samsung Galaxy S8+

## WIP

1. Validation
   - I wanted to listen to changes in the Realm collection and then trigger a validation function in order to enable/disable the swiping/display the feedback buttons. Unfortunately I ran out of time to get this working bug free
2. For more WIP / ToDo please look at `TODO.md`

## BUGS

1. Android Simulator + S8+: The Text labels around the video recording buttons are misplaced to the right on android, which probably caused by the dynamic aspect ratio which resizes the layout
2. Samsung S8+: The first video sometimes does not load, need to introduce an error state here
