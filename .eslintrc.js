module.exports = {
	root: true,
	extends: [
		'@react-native-community',
		'plugin:@typescript-eslint/recommended',
		'plugin:react-hooks/recommended',
		'prettier',
	],
	overrides: [
		{
			parser: '@typescript-eslint/parser',
			plugins: ['@typescript-eslint'],
			files: ['*.tsx', '*.ts'],
			parserOptions: {
				project: './tsconfig.json',
			},
		},
	],
};
