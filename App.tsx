import * as eva from '@eva-design/eva';
import {ApplicationProvider, IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import {Navigation} from 'navigation';
import React from 'react';
import {useColorScheme} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';

export const App: React.FC = () => {
	const isDarkMode = useColorScheme() === 'dark';

	return (
		<>
			<IconRegistry icons={EvaIconsPack} />
			<ApplicationProvider {...eva} theme={isDarkMode ? eva.dark : eva.light}>
				<SafeAreaProvider>
					<Navigation />
				</SafeAreaProvider>
			</ApplicationProvider>
		</>
	);
};
