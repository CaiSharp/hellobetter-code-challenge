Welcome to our Code Challenge Mobile!

Your Task: Build a user interface where a user can go through one of the video chapters of a HelloBetter course and record their responses

Tech Stack: React-Native

In the HelloBetter mobile app users progress through a series of videos which introduce them to informational and educational content. At the end of each video users are asked questions and they have the ability to record their responses as video, audio or text. These responses are later reviewed by our coaches.

Mockup: This is a mockup of a mobile app experience for HelloBetter. It consists of a psychoeducational video delivered by a psychotherapist, at the end of which the user is asked to respond in one of three ways: by recording a selfie video, by recording their answer in voice format, or by writing their answer in text.
https://drive.google.com/file/d/1syMyRmUw1HrBcVQJktC4qYcGMc7nYzaq/view?usp=sharing
You can download this video to have a look.

We want you to build this experience in the mobile app in the modular way by using all the best practices and store the data in the mobile storage and share your codebase with a git private repository. We strongly suggest to not make this repository public.

Thanks

More Info:
• Is the user required to give feedback or can this step be skipped? - This step can be skipped as well, ideally the validation should be configurable.
• Should the user revisit a previous completed course, will they see their previous feedback after each video? - Yes
• Is the user able to exit the series of videos anytime & will they be able to resume where they left off? - It's okay to not track the video itself but if there are 2 videos and the user has completed one of them then the user should start from the second video next time he opens the app.
