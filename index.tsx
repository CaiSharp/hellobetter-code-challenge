import {AppRegistry} from 'react-native';
import {LogBox} from 'react-native';

import {App} from './App';
import {name as appName} from './app.json';

//disable logs only for dev purposes
LogBox.ignoreLogs(['RCTBridge required dispatch_sync to load', 'Constants']);

AppRegistry.registerComponent(appName, () => App);
