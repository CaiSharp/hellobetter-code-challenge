## TODO

1. Bootstrap project

   - init project with typescript [x]
   - add linter, module resolution, prettier, test setup, basic structure, react navigation, ui kitten + eva icons [x]

2. Create List with fake course to enter video tour

   - Create list & card on home [x]
   - configure route in HomeStack [x]
   - disable bottom tab bar on screen []

3. Create swipable pages, intergrate video streaming & player elements for each pages

   - create swipable screens in sequence [x]
   - intergrate video player library -> needs to be able to handle streamed data [x]
     - start / pause / volume / fullscreen / seek [x]
   - populate course with at least 2 videos [x]
   - create ui which pops up at the end of the video [x]

4. Create fullScreen modals for recording answers

   - create video modal, able to record a selfie
     - create modal [x]
     - integrate library for handling recording [x]
     - style [x]
   - create audio modal, able to voice record
     - create modal [x]
     - integrate library for handling audio recording [x]
     - style [x]
   - create text modal, able input text
     - create modal [x]
     - handle textinput focus, scroll & overflow [x]
     - style [x]

5. Save data on the device

   - integrate library to save audio & video recording in file storage [x]
   - intergrate realm [x]
   - create data structure in realm and add reference to stored files in realm, together with text input & progress [x]
   - save progress in video series [x]
   - save text input [x]
   - save audio input [x]
   - save video input [x]
   - add automatic saving method [x]

6. Upon re-entering a course, populate with previous answers & navigate to unfinished video

   - populate text [x]
   - populate recorded video [x]
   - populate recorded voice [x]
   - populate progress [x]

7. Implement optional validation

   - videos should be able to have optional/required answers [/]
     - kinda works, the problem is listening to live DB changes proves to be difficult

8. Tests []

9. Sugar on top

   - create app icon [x]
   - toasts for validation messages
   - video preview
   - animations for UI elements
   - add screen transitions for android
   - save cached videos on device
   - intergrate timer for recording audio
   - E2E to cover native modules
