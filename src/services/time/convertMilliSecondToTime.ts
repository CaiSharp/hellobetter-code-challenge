export const convertMilliSecondsToTime = (
	millis: number | undefined,
): string => {
	if (millis === undefined) return 'time could not be calculated';
	const minutes = Math.floor(millis / 60000);
	const seconds = Number(((millis % 60000) / 1000).toFixed(0));

	return `${minutes}:${seconds < 10 ? '0' : ''}${seconds}`;
};
