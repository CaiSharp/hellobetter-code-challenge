import {Avatar} from '@ui-kitten/components';
import React from 'react';
import {ImageSourcePropType} from 'react-native';

type ListAvatarProps = {
	avatarURL?: string | null;
};

export const ListAvatar: React.FC<ListAvatarProps> = ({avatarURL}) => {
	const imageSource: ImageSourcePropType = avatarURL
		? {uri: avatarURL}
		: require('../../../assets/helloBetterIcon.jpeg');

	return <Avatar shape="square" size="large" source={imageSource} />;
};
