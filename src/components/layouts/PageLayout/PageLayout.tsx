import {Layout, LayoutProps} from '@ui-kitten/components';
import React from 'react';
import {StyleSheet} from 'react-native';

export const PageLayout: React.FC<LayoutProps> = (props) => {
	return (
		<Layout level="2" style={styles.layout} {...props}>
			{props.children}
		</Layout>
	);
};

const styles = StyleSheet.create({
	layout: {
		height: '100%',
		width: '100%',
		paddingHorizontal: 24,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
