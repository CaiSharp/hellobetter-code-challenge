import {useTheme} from '@ui-kitten/components';
import React from 'react';
import {
	NativeSafeAreaViewProps,
	SafeAreaView,
} from 'react-native-safe-area-context';

export const PageSafeArea: React.FC<NativeSafeAreaViewProps> = (props) => {
	const theme = useTheme();
	return (
		<SafeAreaView
			style={{backgroundColor: theme['background-basic-color-2']}}
			edges={['top']}
			{...props}
		>
			{props.children}
		</SafeAreaView>
	);
};
