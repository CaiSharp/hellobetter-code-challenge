import {Text} from '@ui-kitten/components';
import React, {useEffect} from 'react';
import {StyleSheet, ViewStyle} from 'react-native';
import {convertMilliSecondsToTime} from 'services';
import {useElapsedTime} from 'use-elapsed-time';

type TimerProps = {
	isRecording: boolean;
	shouldResetTimer: boolean;
	setShouldResetTimer: (value: boolean) => void;
	style?: ViewStyle;
};

export const Timer: React.FC<TimerProps> = ({
	isRecording,
	style,
	shouldResetTimer,
	setShouldResetTimer,
}) => {
	const {elapsedTime, reset} = useElapsedTime({isPlaying: isRecording});

	useEffect(() => {
		if (shouldResetTimer) {
			reset();
			setShouldResetTimer(false);
		}
	}, [shouldResetTimer, reset, setShouldResetTimer]);
	return (
		<Text category="h5" style={[styles.durationText, style]}>
			{convertMilliSecondsToTime(elapsedTime * 1000)}
		</Text>
	);
};

const styles = StyleSheet.create({
	durationText: {color: 'white', textAlign: 'center'},
});
