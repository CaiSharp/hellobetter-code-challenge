import {useHeaderHeight} from '@react-navigation/elements';
import {Button, Icon, Text} from '@ui-kitten/components';
import {useRealm} from 'db';
import {Camera} from 'expo-camera';
import * as FileSystem from 'expo-file-system';
import {useHideStatusBar, useSaveBeforeRemove} from 'hooks';
import * as _ from 'lodash';
import {HomeNavigationProps} from 'navigation';
import React, {useCallback, useRef, useState} from 'react';
import {StyleSheet, View} from 'react-native';

import {PermissionStatus} from './PermissionStatus/PermissionStatus';
import {Timer} from './Timer/Timer';
import {useCheckDB} from './useCheckDB';
import {useSaveVideoPath} from './useSaveVideoPath';

export const VideoRecording: React.FC<HomeNavigationProps<'VideoRecording'>> =
	({route, navigation}) => {
		const {courseId, videoId} = route.params;

		const [isRecording, setIsRecording] = useState<boolean>(false);
		const [shouldResetTimer, setShouldResetTimer] = useState<boolean>(false);
		const [recordingURI, setRecordingURI] = useState<string>();
		const [cameraRatio, setCameraRatio] = useState<string>();
		const cameraRef = useRef<Camera>(null);
		const headerHeight = useHeaderHeight();

		useHideStatusBar();

		const startRecording = useCallback(async () => {
			try {
				setShouldResetTimer(true);
				setIsRecording(true);

				const data = await cameraRef.current?.recordAsync();

				if (recordingURI) {
					await FileSystem.deleteAsync(recordingURI);
				}

				setRecordingURI(data?.uri);
				setIsRecording(false);
			} catch (err) {
				console.error('Failed to start recording', err);
				setIsRecording(false);
			}
		}, [recordingURI]);

		const stopRecording = () => {
			cameraRef.current?.stopRecording();
		};

		const prepareRatio = useCallback(async () => {
			if (!cameraRatio && cameraRef.current) {
				const ratios = await cameraRef.current.getSupportedRatiosAsync();
				//set the biggest supported ratio
				setCameraRatio(_.last(ratios));
			}
		}, [cameraRatio]);

		const realm = useRealm();

		useCheckDB({realm, courseId, videoId, setRecordingURI});

		const {saveVideoPath} = useSaveVideoPath({
			realm,
			courseId,
			videoId,
			recordingURI,
		});

		useSaveBeforeRemove({navigation, saveFunctions: [saveVideoPath]});

		return (
			<Camera
				ratio={cameraRatio}
				ref={cameraRef}
				style={[
					styles.camera,
					{
						paddingTop: headerHeight,
						paddingBottom: headerHeight - 40,
					},
				]}
				onCameraReady={prepareRatio}
				type="front"
			>
				<Text style={styles.questionText}>
					What's your reason for being here? Why now?
				</Text>
				{recordingURI && (
					<Text style={styles.videoHint}>You've already recorded a video</Text>
				)}
				<PermissionStatus />
				<Timer
					isRecording={isRecording}
					shouldResetTimer={shouldResetTimer}
					setShouldResetTimer={setShouldResetTimer}
					style={styles.timer}
				/>
				<Text style={styles.promptText}>Hit the record button to start!</Text>
				<View style={styles.buttonContainer}>
					<Button
						onPress={isRecording ? stopRecording : startRecording}
						status="danger"
						size="giant"
						style={styles.button}
						accessoryLeft={(props) => (
							<Icon
								{...props}
								name={isRecording ? 'mic-off-outline' : 'mic-outline'}
							/>
						)}
					/>
				</View>
				<Text style={styles.hintText} appearance="hint">
					Don't worry, you can try again later
				</Text>
			</Camera>
		);
	};

const styles = StyleSheet.create({
	camera: {
		flex: 1,
		paddingHorizontal: 24,
		alignItems: 'center',
	},
	buttonContainer: {
		marginTop: 24,
		flexDirection: 'row',
		width: '100%',
		justifyContent: 'center',
	},
	button: {width: 50, height: 50, borderRadius: 100},
	questionText: {
		marginTop: 32,
		fontWeight: 'bold',
		color: 'white',
	},
	promptText: {
		fontWeight: 'bold',
		textAlign: 'center',
		color: 'white',
	},
	hintText: {
		textAlign: 'center',
		marginTop: 16,
		color: 'white',
	},
	timer: {marginTop: 'auto', marginBottom: 16},
	videoHint: {color: 'white', marginTop: 16},
});
