import {Text} from '@ui-kitten/components';
import {Camera} from 'expo-camera';
import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';

export const PermissionStatus: React.FC = () => {
	const [hasPermission, setHasPermission] = useState<boolean>(false);

	useEffect(() => {
		(async () => {
			const {status: camStatus} = await Camera.requestPermissionsAsync();
			const {status: micStatus} =
				await Camera.requestMicrophonePermissionsAsync();
			setHasPermission(camStatus === 'granted' && micStatus === 'granted');
		})();
	}, []);

	return !hasPermission ? (
		<Text style={styles.text}>Please check your camera permissions</Text>
	) : null;
};

const styles = StyleSheet.create({
	text: {marginTop: 'auto'},
});
