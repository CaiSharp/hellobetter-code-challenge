import {VideoCourseSchemaType} from 'db';
import {useCallback} from 'react';

export const useSaveVideoPath = ({
	realm,
	courseId,
	videoId,
	recordingURI,
}: {
	realm: Realm | null | undefined;
	courseId: string;
	videoId: string;
	recordingURI: string | undefined;
}): {saveVideoPath: () => void} => {
	const saveVideoPath = useCallback(() => {
		const savedCollection =
			realm?.objects<VideoCourseSchemaType>('VideoCourse');
		const matchingIdEntries = savedCollection?.filtered(
			`courseId = "${courseId}"`,
		);
		const singleEntry = matchingIdEntries?.find(
			(item) => item.courseId === courseId,
		);

		const videoEntry = singleEntry?.courseVideos.find(
			(el) => el.videoId === videoId,
		);
		if (videoEntry) {
			realm?.write(() => {
				videoEntry.videoFilePath = recordingURI;
			});
		}
	}, [realm, recordingURI, courseId, videoId]);

	return {saveVideoPath};
};
