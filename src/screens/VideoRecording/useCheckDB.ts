import {VideoCourseSchemaType} from 'db';
import {useEffect} from 'react';

export const useCheckDB = ({
	realm,
	courseId,
	videoId,
	setRecordingURI,
}: {
	realm: Realm | null | undefined;
	courseId: string;
	videoId: string;
	setRecordingURI: (uri: string) => void;
}): void => {
	useEffect(() => {
		const savedCollection =
			realm?.objects<VideoCourseSchemaType>('VideoCourse');
		const matchingIdEntries = savedCollection?.filtered(
			`courseId = "${courseId}"`,
		);
		const singleEntry = matchingIdEntries?.find(
			(item) => item.courseId === courseId,
		);

		const videoEntry = singleEntry?.courseVideos.find(
			(el) => el.videoId === videoId,
		);
		if (videoEntry?.videoFilePath) {
			setRecordingURI(videoEntry.videoFilePath);
		}
	}, [realm, courseId, videoId, setRecordingURI]);
};
