import {useHeaderHeight} from '@react-navigation/elements';
import {Button, Icon, Text} from '@ui-kitten/components';
import {PageLayout, PageSafeArea} from 'components';
import {CourseVideoSchemaType, VideoCourseSchemaType, useRealm} from 'db';
import {AVPlaybackStatus, Audio} from 'expo-av';
import * as FileSystem from 'expo-file-system';
import {useSaveBeforeRemove} from 'hooks';
import {HomeNavigationProps} from 'navigation';
import React, {useCallback, useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {convertMilliSecondsToTime} from 'services';

import {useCheckDB} from './useCheckDB';
import {useSaveAudioPath} from './useSaveAudioPath';

export const AudioRecording: React.FC<HomeNavigationProps<'AudioRecording'>> =
	({route, navigation}) => {
		const {courseId, videoId} = route.params;
		const headerHeight = useHeaderHeight();

		const [audioRecording, setAudioRecording] = useState<Audio.Recording>();
		const [recordingURI, setRecordingURI] = useState<string | null>();
		const [audioSound, setAudioSound] = useState<Audio.Sound>();
		const [duration, setDuration] = useState<number>();
		const [isPlaying, setIsPlaying] = useState<boolean>(false);

		const realm = useRealm();

		const startRecording = useCallback(async () => {
			try {
				await Audio.requestPermissionsAsync();
				await Audio.setAudioModeAsync({
					allowsRecordingIOS: true,
					playsInSilentModeIOS: true,
					interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
					staysActiveInBackground: true,
					interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
					shouldDuckAndroid: true,
					playThroughEarpieceAndroid: true,
				});

				if (recordingURI) {
					await FileSystem.deleteAsync(recordingURI);
					setRecordingURI(undefined);
				}

				const {recording} = await Audio.Recording.createAsync(
					Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY,
				);
				setAudioRecording(recording);
			} catch (err) {
				console.error('Failed to start recording', err);
			}
		}, [recordingURI]);

		const stopRecording = useCallback(async () => {
			await audioRecording?.stopAndUnloadAsync();
			const uri = audioRecording?.getURI();
			const {sound} = await Audio.Sound.createAsync(
				{
					uri: uri as string,
				},
				{},
				updatePlayBackStatus,
			);

			setDuration(audioRecording?._finalDurationMillis);
			setAudioSound(sound);
			setAudioRecording(undefined);
			setRecordingURI(uri);
		}, [audioRecording]);

		const updatePlayBackStatus = async (status: AVPlaybackStatus) => {
			if (status?.isLoaded) {
				setIsPlaying(status.isPlaying);
			}
		};

		const playRecording = useCallback(async () => {
			await audioSound?.setPositionAsync(0);
			await audioSound?.playAsync();
		}, [audioSound]);

		useEffect(() => {
			return audioSound
				? () => {
						audioSound.unloadAsync();
				  }
				: undefined;
		}, [audioSound]);

		const createStateFromDB = useCallback(
			async (uri: string, videoEntry: CourseVideoSchemaType) => {
				const {sound, status} = await Audio.Sound.createAsync(
					{
						uri: uri as string,
					},
					{},
					updatePlayBackStatus,
				);

				setRecordingURI(videoEntry.audioFilePath);
				setAudioSound(sound);

				if (status.isLoaded) {
					setDuration(status.durationMillis);
				}
			},
			[],
		);

		useCheckDB({
			realm,
			courseId,
			videoId,
			createStateFromDB,
		});

		const {saveAudioPath} = useSaveAudioPath({
			realm,
			courseId,
			videoId,
			recordingURI,
		});

		useSaveBeforeRemove({navigation, saveFunctions: [saveAudioPath]});

		return (
			<PageSafeArea
				style={{paddingTop: headerHeight}}
				edges={['top', 'bottom']}
			>
				<PageLayout style={styles.layout}>
					<Text style={styles.questionText}>
						What's your reason for being here? Why now?
					</Text>
					<View style={styles.firstLayer}>
						<View style={styles.secondLayer}>
							<View style={styles.thirdLayer}>
								<Text category="h5" style={styles.durationText}>
									{duration ? convertMilliSecondsToTime(duration) : '0:00'}
								</Text>
							</View>
						</View>
					</View>

					<Text style={styles.promptText}>Hit the record button to start!</Text>
					<View style={styles.buttonContainer}>
						<Button
							status="danger"
							size="giant"
							style={styles.button}
							onPress={audioRecording ? stopRecording : startRecording}
							accessoryLeft={(props) => (
								<Icon
									{...props}
									name={audioRecording ? 'mic-off-outline' : 'mic-outline'}
								/>
							)}
						/>
						<Button
							status="danger"
							size="giant"
							style={[styles.button, styles.buttonDistance]}
							onPress={playRecording}
							accessoryLeft={(props) => (
								<Icon
									{...props}
									name={
										isPlaying ? 'pause-circle-outline' : 'play-circle-outline'
									}
								/>
							)}
						/>
					</View>
					<Text style={styles.hintText} appearance="hint">
						Don't worry, you can try again later
					</Text>
				</PageLayout>
			</PageSafeArea>
		);
	};

const styles = StyleSheet.create({
	layout: {
		paddingHorizontal: 24,
		height: '100%',
	},
	button: {width: 50, height: 50, borderRadius: 100},
	buttonDistance: {marginLeft: 10},
	buttonContainer: {
		marginTop: 24,
		flexDirection: 'row',
		width: '100%',
		justifyContent: 'center',
	},
	questionText: {
		fontWeight: 'bold',
	},
	promptText: {fontWeight: 'bold', marginTop: 'auto', textAlign: 'center'},
	hintText: {marginTop: 16, textAlign: 'center'},
	durationText: {color: 'white'},
	firstLayer: {
		marginTop: 'auto',
		alignSelf: 'center',
		justifyContent: 'center',
		alignItems: 'center',
		height: 185,
		width: 185,
		borderRadius: 100,
		backgroundColor: 'rgb(195, 228, 242)',
	},
	secondLayer: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 165,
		width: 165,
		borderRadius: 100,
		backgroundColor: 'rgb(157, 219, 221)',
	},
	thirdLayer: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 150,
		width: 150,
		borderRadius: 100,
		backgroundColor: 'rgb(45, 184, 164)',
	},
});
