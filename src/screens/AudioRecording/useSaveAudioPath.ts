import {VideoCourseSchemaType} from 'db';
import {useCallback} from 'react';

export const useSaveAudioPath = ({
	realm,
	courseId,
	videoId,
	recordingURI,
}: {
	realm: Realm | null | undefined;
	courseId: string;
	videoId: string;
	recordingURI: string | null | undefined;
}): {saveAudioPath: () => void} => {
	const saveAudioPath = useCallback(() => {
		const savedCollection =
			realm?.objects<VideoCourseSchemaType>('VideoCourse');
		const matchingIdEntries = savedCollection?.filtered(
			`courseId = "${courseId}"`,
		);
		const singleEntry = matchingIdEntries?.find(
			(item) => item.courseId === courseId,
		);

		const videoEntry = singleEntry?.courseVideos.find(
			(el) => el.videoId === videoId,
		);
		if (videoEntry && recordingURI) {
			realm?.write(() => {
				videoEntry.audioFilePath = recordingURI;
			});
		}
	}, [realm, recordingURI, courseId, videoId]);

	return {saveAudioPath};
};
