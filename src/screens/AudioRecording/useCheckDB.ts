import {CourseVideoSchemaType, VideoCourseSchemaType} from 'db';
import {useEffect} from 'react';

export const useCheckDB = ({
	realm,
	courseId,
	videoId,
	createStateFromDB,
}: {
	realm: Realm | null | undefined;
	courseId: string;
	videoId: string;
	createStateFromDB: (
		uri: string,
		videoEntry: CourseVideoSchemaType,
	) => Promise<void>;
}): void => {
	useEffect(() => {
		const savedCollection =
			realm?.objects<VideoCourseSchemaType>('VideoCourse');
		const matchingIdEntries = savedCollection?.filtered(
			`courseId = "${courseId}"`,
		);
		const singleEntry = matchingIdEntries?.find(
			(item) => item.courseId === courseId,
		);

		const videoEntry = singleEntry?.courseVideos.find(
			(el) => el.videoId === videoId,
		);
		if (videoEntry?.audioFilePath) {
			createStateFromDB(videoEntry.audioFilePath, videoEntry);
		}
	}, [realm, courseId, videoId, createStateFromDB]);
};
