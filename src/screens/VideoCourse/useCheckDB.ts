import {VideoCourseSchemaType} from 'db';
import {useEffect} from 'react';
import {Course} from 'types';

export const useCheckDB = ({
	realm,
	courseId,
	fakeCourse,
	setSavedData,
	setProgress,
}: {
	realm: Realm | null | undefined;
	courseId: string;
	fakeCourse: Course;
	setSavedData: (value: VideoCourseSchemaType | undefined) => void;
	setProgress: (value: number) => void;
}): void => {
	useEffect(() => {
		if (realm && !realm.isClosed) {
			const savedCollection =
				realm.objects<VideoCourseSchemaType>('VideoCourse');
			const matchingIdEntries = savedCollection.filtered(
				`courseId = "${courseId}"`,
			);
			const singleEntry = matchingIdEntries.find(
				(item) => item.courseId === courseId,
			);

			setSavedData(singleEntry?.toJSON() as VideoCourseSchemaType);

			if (singleEntry) {
				setProgress(singleEntry.progress);
			}

			if (!singleEntry) {
				realm.write(() => {
					realm.create<VideoCourseSchemaType>('VideoCourse', {
						courseId,
						courseVideos: fakeCourse.courseVideos,
						progress: 0,
					});
				});
			}
		}
	}, [realm, courseId, fakeCourse, setProgress, setSavedData]);
};
