import {VideoCourseSchemaType} from 'db';
import {useCallback} from 'react';
import PagerView from 'react-native-pager-view';
import {Course} from 'types';

export const useCheckValid = ({
	savedData,
	fakeCourse,
	swiperRef,
}: {
	savedData: VideoCourseSchemaType | undefined;
	fakeCourse: Course;
	swiperRef: React.RefObject<PagerView>;
}): {checkValid: (currentProgress: number) => void} => {
	const checkValid = useCallback(
		(currentProgress: number) => {
			const currentVideo = savedData?.courseVideos[currentProgress];

			if (
				currentVideo?.audioFilePath ||
				currentVideo?.textInput ||
				currentVideo?.videoFilePath
			) {
				swiperRef.current?.setScrollEnabled(true);
			} else {
				if (fakeCourse.courseVideos[currentProgress].required) {
					swiperRef.current?.setScrollEnabled(false);
				}
			}
		},
		[savedData, fakeCourse, swiperRef],
	);

	return {checkValid};
};
