import {VideoCourseSchemaType} from 'db';
import {useCallback} from 'react';

export const useSaveProgress = ({
	realm,
	courseId,
}: {
	realm: Realm | null | undefined;
	courseId: string;
}): {saveProgress: (newProgress: number) => void} => {
	const saveProgress = useCallback(
		(newProgress: number) => {
			if (realm && !realm.isClosed) {
				const savedCollection =
					realm.objects<VideoCourseSchemaType>('VideoCourse');
				const matchingIdEntries = savedCollection?.filtered(
					`courseId = "${courseId}"`,
				);
				const singleEntry = matchingIdEntries?.find(
					(item) => item.courseId === courseId,
				);

				if (singleEntry) {
					realm.write(() => {
						singleEntry.progress = newProgress;
					});
				}
			}
		},
		[realm, courseId],
	);
	return {saveProgress};
};
