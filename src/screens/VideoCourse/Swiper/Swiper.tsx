import React from 'react';
import {StyleSheet} from 'react-native';
import PagerView from 'react-native-pager-view';
import {Course} from 'types';

import {VideoPlayer} from './VideoPlayer/VideoPlayer';

type SwiperProps = {
	swiperRef: React.RefObject<PagerView>;
	course: Course;
	progress: number;
	setProgress: (value: number) => void;
	saveProgress: (value: number) => void;
	checkValid: (value: number) => void;
};

export const Swiper: React.FC<SwiperProps> = ({
	swiperRef,
	course,
	progress,
	setProgress,
	saveProgress,
	checkValid,
}) => {
	const elementsToRender = course.courseVideos.map((el, index) => (
		<VideoPlayer key={index} courseVideo={el} />
	));
	return (
		<PagerView
			ref={swiperRef}
			initialPage={progress}
			style={styles.swiper}
			onPageSelected={(event) => {
				setProgress(event.nativeEvent.position);
				saveProgress(event.nativeEvent.position);
				//checkValid(event.nativeEvent.position);
			}}
		>
			{elementsToRender}
		</PagerView>
	);
};

const styles = StyleSheet.create({
	swiper: {
		height: '100%',
	},
});
