import {AVPlaybackStatus, Video} from 'expo-av';
import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {CourseVideo} from 'types';

import {FeedbackPrompt} from './FeedbackPrompt/FeedbackPrompt';

type VideoPlayer = {
	courseVideo: CourseVideo;
};

export const VideoPlayer: React.FC<VideoPlayer> = ({courseVideo}) => {
	const video = React.useRef<Video>(null);
	const [playbackStatus, setPlaybackStatus] = useState<AVPlaybackStatus>();
	const [showFeedbackUI, setShowFeedbackUI] = useState(false);

	const {videoId, videoURL} = courseVideo;

	useEffect(() => {
		if (playbackStatus?.isLoaded && playbackStatus.didJustFinish) {
			setShowFeedbackUI(true);
		}
	}, [playbackStatus]);

	return (
		<View collapsable={false} style={styles.container}>
			<Video
				ref={video}
				source={{
					uri: videoURL,
				}}
				useNativeControls
				style={styles.video}
				resizeMode="contain"
				onPlaybackStatusUpdate={(status) => setPlaybackStatus(() => status)}
			/>

			{/*animate this*/}
			<FeedbackPrompt show={showFeedbackUI} videoId={videoId} />
		</View>
	);
};

const styles = StyleSheet.create({
	container: {
		height: '100%',
		position: 'relative',
	},
	video: {flex: 1},
});
