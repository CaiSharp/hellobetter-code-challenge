import {useNavigation, useRoute} from '@react-navigation/native';
import {Button, Icon} from '@ui-kitten/components';
import {HomeNavigationProp, HomeRouteProp} from 'navigation';
import React from 'react';
import {StyleSheet, View} from 'react-native';

type FeedbackPromptProps = {
	show: boolean;
	videoId: string;
};

export const FeedbackPrompt: React.FC<FeedbackPromptProps> = ({
	show,
	videoId,
}) => {
	const navigation = useNavigation<HomeNavigationProp<'VideoCourse'>>();
	const route = useRoute<HomeRouteProp<'VideoCourse'>>();

	const {courseId} = route.params;

	return show ? (
		<View style={styles.container}>
			<Button
				size="large"
				accessoryLeft={(props) => <Icon {...props} name="video-outline" />}
				onPress={() => {
					navigation.navigate('VideoRecording', {courseId, videoId});
				}}
			/>
			<Button
				size="large"
				accessoryLeft={(props) => <Icon {...props} name="mic-outline" />}
				onPress={() => {
					navigation.navigate('AudioRecording', {courseId, videoId});
				}}
			/>
			<Button
				size="large"
				accessoryLeft={(props) => <Icon {...props} name="text" />}
				onPress={() => {
					navigation.navigate('TextRecording', {courseId, videoId});
				}}
			/>
		</View>
	) : null;
};

const styles = StyleSheet.create({
	container: {
		width: '100%',
		position: 'absolute',
		bottom: 100,
		flexDirection: 'row',
		justifyContent: 'space-evenly',
	},
});
