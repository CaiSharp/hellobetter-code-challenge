import {useHeaderHeight} from '@react-navigation/elements';
import {PageSafeArea} from 'components';
import {VideoCourseSchemaType, useRealm} from 'db';
import {useHideStatusBar} from 'hooks';
import {HomeNavigationProps} from 'navigation';
import React, {useMemo, useRef, useState} from 'react';
import {StyleSheet} from 'react-native';
import PagerView from 'react-native-pager-view';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Course, CourseVideo} from 'types';

import {Swiper} from './Swiper/Swiper';
import {useCheckDB} from './useCheckDB';
import {useCheckValid} from './useCheckValid';
import {useSaveProgress} from './useSaveProgress';

//query from backend with courseID
const courseVideos: CourseVideo[] = [
	{
		videoId: '1',
		videoURL:
			'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4',
		required: true,
	},
	{
		videoId: '2',
		videoURL:
			'https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4',
		required: false,
	},
];

export const VideoCourse: React.FC<HomeNavigationProps<'VideoCourse'>> = ({
	route,
}) => {
	const {courseId} = route.params;
	const swiperRef = useRef<PagerView>(null);
	const headerHeight = useHeaderHeight();
	const insets = useSafeAreaInsets();
	const [progress, setProgress] = useState<number>(0);
	const [savedData, setSavedData] = useState<
		VideoCourseSchemaType | undefined
	>();

	const fakeCourse: Course = useMemo(
		() => ({
			courseId,
			courseVideos,
		}),
		[courseId],
	);

	useHideStatusBar();

	const realm = useRealm();

	useCheckDB({realm, courseId, fakeCourse, setSavedData, setProgress});

	const {saveProgress} = useSaveProgress({realm, courseId});

	const {checkValid} = useCheckValid({savedData, fakeCourse, swiperRef});

	return (
		<PageSafeArea
			mode="margin"
			style={[
				styles.safeArea,
				{
					paddingTop: headerHeight,

					paddingBottom: insets.bottom,
				},
			]}
			edges={['left']}
		>
			<Swiper
				key={`${savedData?.courseId}`}
				course={fakeCourse}
				swiperRef={swiperRef}
				progress={progress}
				setProgress={setProgress}
				saveProgress={saveProgress}
				checkValid={checkValid}
			/>
		</PageSafeArea>
	);
};

const styles = StyleSheet.create({
	safeArea: {
		backgroundColor: 'black',
	},
});
