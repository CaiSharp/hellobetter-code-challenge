import {VideoCourseSchemaType} from 'db';
import {useCallback} from 'react';

export const useSaveTextInput = ({
	realm,
	courseId,
	videoId,
	value,
}: {
	realm: Realm | null | undefined;
	courseId: string;
	videoId: string;
	value: string | undefined;
}): {saveTextInput: () => void} => {
	const saveTextInput = useCallback(() => {
		const savedCollection =
			realm?.objects<VideoCourseSchemaType>('VideoCourse');
		const matchingIdEntries = savedCollection?.filtered(
			`courseId = "${courseId}"`,
		);
		const singleEntry = matchingIdEntries?.find(
			(item) => item.courseId === courseId,
		);

		const videoEntry = singleEntry?.courseVideos.find(
			(el) => el.videoId === videoId,
		);
		if (videoEntry) {
			realm?.write(() => {
				videoEntry.textInput = value;
			});
		}
	}, [realm, value, courseId, videoId]);

	return {saveTextInput};
};
