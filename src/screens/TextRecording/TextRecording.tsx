import {useHeaderHeight} from '@react-navigation/elements';
import {Input} from '@ui-kitten/components';
import {PageLayout, PageSafeArea} from 'components';
import {useRealm} from 'db';
import {useSaveBeforeRemove} from 'hooks';
import {HomeNavigationProps} from 'navigation';
import React, {useState} from 'react';
import {
	Keyboard,
	KeyboardAvoidingView,
	Platform,
	Pressable,
	StyleSheet,
} from 'react-native';

import {useCheckDB} from './useCheckDB';
import {useSaveTextInput} from './useSaveTextInput';

export const TextRecording: React.FC<HomeNavigationProps<'TextRecording'>> = ({
	navigation,
	route,
}) => {
	const {courseId, videoId} = route.params;
	const headerHeight = useHeaderHeight();
	const [value, setValue] = useState<string>();

	const realm = useRealm();

	useCheckDB({realm, courseId, videoId, setValue});

	const {saveTextInput} = useSaveTextInput({realm, value, courseId, videoId});

	useSaveBeforeRemove({navigation, saveFunctions: [saveTextInput]});

	return (
		<KeyboardAvoidingView
			style={styles.keyboardView}
			behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
		>
			<Pressable onPress={Keyboard.dismiss}>
				<PageSafeArea
					style={{paddingTop: headerHeight}}
					edges={['top', 'bottom']}
				>
					<PageLayout style={styles.layout}>
						<Input
							value={value}
							onChangeText={setValue}
							placeholder="Start writing here..."
							multiline
						/>
					</PageLayout>
				</PageSafeArea>
			</Pressable>
		</KeyboardAvoidingView>
	);
};

const styles = StyleSheet.create({
	keyboardView: {flex: 1},
	layout: {
		justifyContent: 'flex-start',
		paddingHorizontal: 24,
	},
});
