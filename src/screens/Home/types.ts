import {ImageURISource} from 'react-native';

export type Course = {
	title: string;
	description: string;
	previewImage?: ImageURISource['uri'] | null;
	courseId: string;
};
