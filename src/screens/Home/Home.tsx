import {Divider, List, ListItem, Text} from '@ui-kitten/components';
import {ListAvatar, PageLayout, PageSafeArea} from 'components';
import {HomeNavigationProps} from 'navigation';
import React from 'react';
import {StyleSheet} from 'react-native';

import {Course} from './types';

//query from backend
const exampleCourses: Course[] = [
	{
		title: 'Taking care of your personal mindspace',
		description: 'Learn more about meditation',
		previewImage:
			'https://www.lvlfi.com/wp-content/uploads/2020/09/Blog-56.jpg',
		courseId: '1',
	},
	{
		title: 'How to listen to your inner self',
		description: 'Learn more about mindset',
		previewImage: null,
		courseId: '2',
	},
];

export const Home: React.FC<HomeNavigationProps<'Home'>> = ({navigation}) => {
	return (
		<PageSafeArea>
			<PageLayout>
				<Text style={styles.heading} category="h2">
					Welcome back,{'\n'}Dave
				</Text>
				<List
					style={styles.list}
					data={exampleCourses}
					ItemSeparatorComponent={Divider}
					renderItem={({item}) => (
						<ListItem
							title={item.title}
							description={item.description}
							onPress={() =>
								navigation.navigate('VideoCourse', {courseId: item.courseId})
							}
							accessoryLeft={() => <ListAvatar avatarURL={item.previewImage} />}
						/>
					)}
				/>
			</PageLayout>
		</PageSafeArea>
	);
};

const styles = StyleSheet.create({
	list: {
		width: '100%',
	},
	heading: {
		marginTop: 24,
		marginBottom: 32,
		textAlign: 'left',
		width: '100%',
	},
});
