import {
	BottomTabNavigationProp,
	createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import {CompositeNavigationProp, RouteProp} from '@react-navigation/native';
import {Icon, Text, useTheme} from '@ui-kitten/components';
import {AppRootNavigationProp} from 'navigation/stacks/AppRoot';
import {HomeParamList} from 'navigation/stacks/Home';
import React from 'react';
import {StyleSheet} from 'react-native';
import {Home} from 'screens';

//TABBAR
export type TabParamList = {
	HomeStack: {
		screen?: keyof HomeParamList;
		params?: HomeParamList[keyof HomeParamList];
	};
};

export type TabNavigationProp<RouteName extends keyof TabParamList> =
	CompositeNavigationProp<
		AppRootNavigationProp<'HomeStack'>,
		BottomTabNavigationProp<TabParamList, RouteName>
	>;

type TabRouteProp<RouteName extends keyof TabParamList> = RouteProp<
	TabParamList,
	RouteName
>;

// import this in your screens as navigation prop
export type TabNavigationProps<RouteName extends keyof TabParamList> = {
	navigation: TabNavigationProp<RouteName>;
	route: TabRouteProp<RouteName>;
};

const Tab = createBottomTabNavigator();

export const TabBar = (): JSX.Element => {
	const theme = useTheme();
	return (
		<Tab.Navigator
			screenOptions={({route}) => ({
				tabBarIcon: ({focused, size}) => {
					let iconName;

					switch (route.name) {
						case 'HomeStack':
							iconName = 'home';
							break;
					}

					return (
						<Icon
							style={{width: size, height: size}}
							fill={
								focused ? theme['text-primary-color'] : theme['text-hint-color']
							}
							name={iconName}
						/>
					);
				},

				tabBarLabel: ({focused}) => {
					let labelText;

					switch (route.name) {
						case 'HomeStack':
							labelText = 'Home';
							break;
					}

					return (
						<Text
							style={[
								styles.labelText,
								{
									color: focused
										? theme['text-primary-color']
										: theme['text-hint-color'],
								},
							]}
						>
							{labelText}
						</Text>
					);
				},
				headerShown: false,
			})}
		>
			<Tab.Screen name="HomeStack" component={Home} />
		</Tab.Navigator>
	);
};

const styles = StyleSheet.create({
	labelText: {
		fontSize: 12,
	},
});
