import {
	DarkTheme,
	DefaultTheme,
	NavigationContainer,
	Theme,
} from '@react-navigation/native';
import {useTheme} from '@ui-kitten/components';
import React from 'react';
import {useColorScheme} from 'react-native';

import {AppRoot} from '../stacks/AppRoot';

export const Navigation: React.FC = () => {
	const theme = useTheme();
	const isDarkMode = useColorScheme() === 'dark';

	const standardTheme = isDarkMode ? DarkTheme : DefaultTheme;

	const customTheme: Theme = {
		dark: isDarkMode,
		colors: {
			...standardTheme.colors,
			primary: theme['color-primary-700'],
			background: theme['background-basic-color-2'],
			text: theme['color-primary-100'],
			card: theme['background-basic-color-1'],
		},
	};

	return (
		<NavigationContainer theme={customTheme}>{AppRoot()}</NavigationContainer>
	);
};
