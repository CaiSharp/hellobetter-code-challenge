import {CompositeNavigationProp, RouteProp} from '@react-navigation/native';
import {
	NativeStackNavigationProp,
	createNativeStackNavigator,
} from '@react-navigation/native-stack';
import {Button, Icon} from '@ui-kitten/components';
import {TabBar, TabNavigationProp} from 'navigation/tabs/Tab';
import React from 'react';
import {
	AudioRecording,
	TextRecording,
	VideoCourse,
	VideoRecording,
} from 'screens';

import {createTransparentHeader} from '../headers';

//Home
export type HomeParamList = {
	Home: undefined;
	VideoCourse: {courseId: string};
	AudioRecording: {courseId: string; videoId: string};
	TextRecording: {courseId: string; videoId: string};
	VideoRecording: {courseId: string; videoId: string};
};

export type HomeNavigationProp<RouteName extends keyof HomeParamList> =
	CompositeNavigationProp<
		TabNavigationProp<'HomeStack'>,
		NativeStackNavigationProp<HomeParamList, RouteName>
	>;

export type HomeRouteProp<RouteName extends keyof HomeParamList> = RouteProp<
	HomeParamList,
	RouteName
>;

// import this in your screens as navigation prop
export type HomeNavigationProps<RouteName extends keyof HomeParamList> = {
	navigation: HomeNavigationProp<RouteName>;
	route: HomeRouteProp<RouteName>;
};

const HomeStackNav = createNativeStackNavigator<HomeParamList>();

export const HomeStack = (): JSX.Element => (
	<HomeStackNav.Navigator screenOptions={{headerShown: false}}>
		<HomeStackNav.Group>
			<HomeStackNav.Screen name="Home" component={TabBar} />
			<HomeStackNav.Screen
				name="VideoCourse"
				component={VideoCourse}
				options={(props) => createTransparentHeader(props, {showHelp: false})}
			/>
		</HomeStackNav.Group>
		<HomeStackNav.Group
			screenOptions={{
				presentation: 'fullScreenModal',
			}}
		>
			<HomeStackNav.Screen
				name="VideoRecording"
				component={VideoRecording}
				options={(props) => createTransparentHeader(props, {showHelp: true})}
			/>
			<HomeStackNav.Screen
				name="AudioRecording"
				component={AudioRecording}
				options={(props) => createTransparentHeader(props, {showHelp: true})}
			/>
			<HomeStackNav.Screen
				name="TextRecording"
				component={TextRecording}
				options={(props) => createTransparentHeader(props, {showHelp: true})}
			/>
		</HomeStackNav.Group>
	</HomeStackNav.Navigator>
);
