import {RouteProp} from '@react-navigation/native';
import {
	NativeStackNavigationProp,
	createNativeStackNavigator,
} from '@react-navigation/native-stack';
import React from 'react';

import {HomeParamList, HomeStack} from './Home';

//ROOT STACK FOR MAIN APP & MODALS
type AppRootParamList = {
	HomeStack: {
		screen?: keyof HomeParamList;
		params?: HomeParamList[keyof HomeParamList];
	};
};

export type AppRootNavigationProp<RouteName extends keyof AppRootParamList> =
	NativeStackNavigationProp<AppRootParamList, RouteName>;

type AppRootRouteProp<RouteName extends keyof AppRootParamList> = RouteProp<
	AppRootParamList,
	RouteName
>;

// import this in your screens as navigation prop
export type AppRootNavigationProps<RouteName extends keyof AppRootParamList> = {
	navigation: AppRootNavigationProp<RouteName>;
	route: AppRootRouteProp<RouteName>;
};

const AppRootStackNav = createNativeStackNavigator<AppRootParamList>();

export const AppRoot = (): JSX.Element => (
	<AppRootStackNav.Navigator screenOptions={{headerShown: false}}>
		<AppRootStackNav.Screen name="HomeStack" children={HomeStack} />
	</AppRootStackNav.Navigator>
);
