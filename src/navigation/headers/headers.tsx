import {NativeStackNavigationOptions} from '@react-navigation/native-stack';
import {Button, Icon} from '@ui-kitten/components';
import React from 'react';
import {Alert} from 'react-native';

export const createTransparentHeader = (
	props: {
		route: any;
		navigation: any;
	},
	options: {showHelp: boolean},
): NativeStackNavigationOptions => ({
	headerShown: true,
	headerTransparent: true,
	headerShadowVisible: false,
	title: '',
	headerLeft: () => (
		<Button
			size="large"
			appearance="ghost"
			onPress={() => props.navigation.goBack()}
			accessoryLeft={(iconProps) => (
				<Icon name="arrow-back-outline" {...iconProps} />
			)}
		/>
	),
	headerRight: () => {
		if (options.showHelp) {
			return (
				<Button
					size="large"
					appearance="ghost"
					onPress={() => Alert.alert('Your help menu here')}
					accessoryLeft={(iconProps) => (
						<Icon name="question-mark-circle-outline" {...iconProps} />
					)}
				/>
			);
		}
	},
});

export const createDefaultHeader = (
	props: {
		route: any;
		navigation: any;
	},
	options: {showHelp: boolean},
): NativeStackNavigationOptions => ({
	headerShown: true,
	title: '',
	headerLeft: () => (
		<Button
			size="large"
			appearance="ghost"
			onPress={() => props.navigation.goBack()}
			accessoryLeft={(iconProps) => (
				<Icon name="arrow-back-outline" {...iconProps} />
			)}
		/>
	),
	headerRight: () => {
		if (options.showHelp) {
			return (
				<Button
					size="large"
					appearance="ghost"
					onPress={() => Alert.alert('Your help menu here')}
					accessoryLeft={(iconProps) => (
						<Icon name="question-mark-circle-outline" {...iconProps} />
					)}
				/>
			);
		}
	},
});
