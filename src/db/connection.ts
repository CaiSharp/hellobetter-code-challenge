import {useEffect, useState} from 'react';
import Realm from 'realm';

import {migrateRealm} from './migration';
import {CourseVideoSchema, VideoCourseSchema} from './schema';

const latestSchemaVersion = 1;
const schemas = [VideoCourseSchema, CourseVideoSchema];

export const useRealm = (): Realm | undefined | null => {
	const [realm, setRealm] = useState<Realm | null>();

	useEffect(() => {
		(async () => {
			if (!realm) {
				const initRealm = await Realm.open({
					schema: schemas,
					schemaVersion: latestSchemaVersion,
					migration: migrateRealm,
				});
				setRealm(initRealm);
			}
		})().catch((err) => console.warn(err));

		return () => realm?.close();
	}, [realm]);

	return realm;
};
