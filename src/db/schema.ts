export const CourseVideoSchema = {
	name: 'CourseVideo',
	properties: {
		videoId: 'string',
		audioFilePath: 'string?',
		videoFilePath: 'string?',
		textInput: 'string?',
	},
};

//user type & schema
export const VideoCourseSchema = {
	name: 'VideoCourse',
	properties: {
		courseId: 'string',
		courseVideos: 'CourseVideo[]',
		progress: 'int?',
	},
};

export type CourseVideoSchemaType = {
	videoId: string;
	audioFilePath?: string;
	videoFilePath?: string;
	textInput?: string;
};

export type VideoCourseSchemaType = {
	courseId: string;
	courseVideos: CourseVideoSchemaType[];
	progress: number;
};
