export type CourseVideo = {
	videoId: string;
	videoURL: string;
	required: boolean;
};

export type Course = {
	courseId: string;
	courseVideos: CourseVideo[];
};
