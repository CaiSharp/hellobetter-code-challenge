import {HomeNavigationProp} from 'navigation';
import {useEffect} from 'react';

export const useSaveBeforeRemove = ({
	navigation,
	saveFunctions,
}: {
	navigation: HomeNavigationProp<
		'AudioRecording' | 'TextRecording' | 'VideoCourse' | 'VideoRecording'
	>;
	saveFunctions: {(): void}[];
}): void => {
	useEffect(() => {
		const unsubscribe = navigation.addListener('beforeRemove', (event) => {
			saveFunctions.forEach((fn) => fn());
			navigation.dispatch(event.data.action);
		});
		return unsubscribe;
	}, [navigation, saveFunctions]);
};
