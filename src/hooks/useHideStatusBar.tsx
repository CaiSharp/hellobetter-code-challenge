import {useEffect} from 'react';
import {StatusBar} from 'react-native';

export const useHideStatusBar = (): void => {
	useEffect(() => {
		StatusBar.setHidden(true);
		return () => {
			StatusBar.setHidden(false);
		};
	}, []);
};
